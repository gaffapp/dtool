var fse = require('fs-extra');
const fs = require("fs");
const chalk = require('chalk');
const path = require('./path');

const cyan = chalk.cyan;
const red = chalk.bold.red;
const green = chalk.bold.green;

module.exports = class CopyFromTo {

    constructor() {
        this.init();
    }


    /**
     * *************************************************************************
     * Init code
     * *************************************************************************
     */
    init() {
        var _paths = this.routes();

        for (let i = 0; i < _paths.length; i++) {
            const element = _paths[i];
            if (this.dirExist(element.origin)) {
                this.copyRecursive(element.origin, element.destinity);
            }

        }
    };

    /**
     * Copy folders and files
     */
    async copyRecursive(_origin, _destiny) {
        try {
            await fse.copy(_origin, _destiny);
            console.log(cyan('------------------------------------------------------'));
            console.log(cyan('Componente: ') + _origin);
            console.log(cyan('SPA: ') + _destiny);
            console.log(cyan('Copiado:') + green('  OK '));
            console.log(cyan('------------------------------------------------------'));
        } catch (err) {
            console.log(red('**************************  ERROR  ******************************'));
            console.error(err);
            console.log(red('*****************************************************************'));
        }
    };

    /**
     * Directory exist
     */
    dirExist(_path) {

        try {
            return fs.statSync(_path).isDirectory();
        }
        catch (e) {

            if (e.code == 'ENOENT') { // no such file or directory. File really does not exist
                console.log(red("No existe: ") + _path);
            } else {
                console.log(red(e));
            }

            return false;
        }
    };

    /**
     * Construct origin and destinity path
     */
    routes() {
        var _path = [];

        const folders = path.folders;
        const comp = path.component;
        const spa = path.spa;

        folders.forEach((element) => {
            _path.push({
                origin: comp.path + element.origin + comp.subfolder,
                destinity: spa.path + spa.nodeModule + element.destinity + spa.subfolder
            });
        });

        return _path;
    };
};
