const chalk = require('chalk');
const yosay = require('yosay');
const changeCase = require('change-case');
const fs = require("fs");
const readline = require("readline");
const prompts = require('prompts');
const generateComponents = require("./components/generate-components/generateComponents");
const copyFromTo = require("./components/copy-from-to/copyFromTo");


const init = () => {
  console.log(yosay(`Welcome to the dazzling ${chalk.red('DTool')}!`));
  const questions = [
    {
      type: 'select',
      name: 'option',
      message: 'Elige una opción: ',
      choices: [
        { title: 'Nuevo componente', value: 1 },
        { title: 'Copiar componente', value: 2 },
        { title: 'Eliminar node_module', value: 3 }
      ],
    }
  ];

  (async () => {
    const response = await prompts(questions);
    switchOp(response.option);
  })();
};

var switchOp = (_op) => {
  switch (_op) {
    case 1: // Nuevo componente
      this.generateComponents = new generateComponents();
      break;
    case 2: // Copiar componente
      this.copyFolder = new copyFromTo();
      break;

    default:
      console.log(red("Opción invalida..."));
      break;
  }
};

// start app
init();